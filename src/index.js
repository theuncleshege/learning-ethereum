const Web3 = require('web3');
const web3 = new Web3('http://localhost:7545');
const RipToken = require('./abis/RipToken.json');

let ripToken;
let accounts;

async function loadContract() {
  accounts = await web3.eth.getAccounts();
  const networkId = await web3.eth.net.getId();
  const ripTokenData = RipToken.networks[networkId];

  ripToken = new web3.eth.Contract(RipToken.abi, ripTokenData.address);
}

async function getBalance(address) {
  const balance = await ripToken.methods.balanceOf(address).call();
  console.log(`Balance of address: ${address} is ${web3.utils.fromWei(balance)} RIP`);
}

async function transferTokens(sender, receipient, amount = '1') {
  const tokens = web3.utils.toWei(amount);

  const result = await ripToken.methods
    .transfer(receipient, tokens)
    .send({ from: sender });

  console.log(`Transfer successful! Hash: ${result.transactionHash}`);
}

async function getBlockNumber() {
  const latestBlockNumber = await web3.eth.getBlockNumber();
  console.log(latestBlockNumber);
  return latestBlockNumber;
}

async function init() {
  await loadContract();
  await getBlockNumber();
  await getBalance(accounts[0]);
  await transferTokens(accounts[0], accounts[2]);
  await getBalance(accounts[2]);
  await getBalance(accounts[0]);
}

init();
