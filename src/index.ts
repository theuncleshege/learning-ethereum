import Web3 from "web3";
import RipToken from './abis/RipToken.json';

let web3: Web3;
let ripToken;
let accounts: string[];

async function loadWeb3() {
  web3 = new Web3('http://localhost:7545');
  accounts = await web3.eth.getAccounts();
}

async function loadContract() {
  const networkId = await web3.eth.net.getId();
  const ripTokenData = RipToken.networks[networkId];

  ripToken = new web3.eth.Contract((RipToken as any).abi, ripTokenData.address);
}

async function getBalance(address: string) {
  const balance = await ripToken.methods.balanceOf(address).call();
  console.log(`Balance of address: ${address} is ${web3.utils.fromWei(balance)} RIP`);
}

async function transferTokens(sender: string, receipient: string, amount: number = 1) {
  const tokens = web3.utils.toWei(amount.toString());

  const result = await ripToken.methods
    .transfer(receipient, tokens)
    .send({ from: sender });

  console.log(`Transfer successful! Hash: ${result.transactionHash}`);
}

async function getBlockNumber() {
  const latestBlockNumber = await web3.eth.getBlockNumber();
  console.log(latestBlockNumber);
  return latestBlockNumber;
}

async function init() {
  await loadWeb3();
  await loadContract();
  await getBlockNumber();
  await getBalance(accounts[0]);
  await transferTokens(accounts[0], accounts[2]);
  await getBalance(accounts[2]);
  await getBalance(accounts[0]);
}

init();
