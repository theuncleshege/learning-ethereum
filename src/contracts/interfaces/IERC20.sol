// SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 <0.8.0;

// Standard ERC-20 token interface
interface IERC20 {

	// Returns the amount of tokens in existence. This function is a getter and does not modify the state of the contract. Keep in mind that there is no floats in Solidity. Therefore most tokens adopt 18 decimals and wil return the total supply and other results as followed 1000000000000000000 for 1 token. Not every tokens has 18 decimals and this is something you really need to watch for when dealing with tokens.
	function totalSupply() external view returns (uint256);

	// Returns the amount of tokens owned by an address (`account`). This function is a getter and does not modify the state of the contract.
  	function balanceOf(address account) external view returns (uint256);

	// The ERC-20 standard allow an address to give an alowance to another address to be able to retrieve tokens from it. This getter returns the remaining number of tokens that the `spender` will be allowed to spend on behalf of `owner`. This function is a getter and does not modify the state of the contract and should return 0 by default.
    function allowance(address owner, address spender) external view returns (uint256);


	// Moves the `amount` of tokens from the function caller address (msg.sender) to the recipient address. This function emits the `Transfer` event. It returns true if the transfer was possible.
    function transfer(address recipient, uint256 amount) external returns (bool);


	// Set the amount of `allowance` the `spender` is allowed to transfer from the function caller (`msg.sender`) balance. This function emits the `Approval` event. The function returns wether the allowance was successfuly set.
    function approve(address spender, uint256 amount) external returns (bool);

	// Moves the `amount` of tokens from `sender` to `recipient` using the allowance mechanism. amount is then deducted from the caller’s allowance. This function emits the `Transfer` event.
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);


	// This event is emitted when the amount of tokens (`value`) is sent from the `from` address to the `to` address.
	// In the case of minting new tokens, the transfer is usually from the 0x00..0000 address while in the case of burning tokens the transfer is to 0x00..0000.
    event Transfer(address indexed from, address indexed to, uint256 value);

	// This event is emitted when the amount of tokens (`value`) is approved by the `owner` to be used by the `spender`.
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
