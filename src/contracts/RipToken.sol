// SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 <0.8.0;

import "./interfaces/IERC20.sol";
import "./libraries/SafeMath.sol";

// A Basic Implementation of ERC-20 Token
contract RipToken is IERC20 {
	using SafeMath for uint256;

    string public constant name = "RipToken";
    string public constant symbol = "RIP";
    uint8 public constant decimals = 18;
    uint256 public _totalSupply = 1000000 ether; // 1 million tokens
    // uint256 public _totalSupply = 1000000000000000000000000; // 1 million tokens

	mapping(address => uint256) balances;
    mapping(address => mapping (address => uint256)) allowed;


	constructor() public {
		balances[msg.sender] = _totalSupply;
    }

    function totalSupply() public override view returns (uint256) {
    	return _totalSupply;
    }

    function balanceOf(address tokenOwner) public override view returns (uint256) {
        return balances[tokenOwner] ;
    }

    function transfer(address receiver, uint256 numTokens) public override returns (bool) {
        require(numTokens <= balances[msg.sender], "Sender has insufficient balance.");

        balances[msg.sender] = balances[msg.sender].sub(numTokens);
        balances[receiver] = balances[receiver].add(numTokens);

        emit Transfer(msg.sender, receiver, numTokens);

		return true;
    }

    function approve(address spender, uint256 numTokens) public override returns (bool) {
        allowed[msg.sender][spender] = numTokens;

        emit Approval(msg.sender, spender, numTokens);

        return true;
    }

    function allowance(address owner, address spender) public override view returns (uint) {
        return allowed[owner][spender];
    }

    function transferFrom(address owner, address buyer, uint256 numTokens) public override returns (bool) {
        require(numTokens <= balances[owner], "Owner has insufficient balance.");
        require(numTokens <= allowed[owner][msg.sender]);

        balances[owner] = balances[owner].sub(numTokens);
        allowed[owner][msg.sender] = allowed[owner][msg.sender].sub(numTokens);
        balances[buyer] = balances[buyer].add(numTokens);

		emit Transfer(owner, buyer, numTokens);

        return true;
    }
}
